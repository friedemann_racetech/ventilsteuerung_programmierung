/*
 * calculation.c
 *
 *  Created on: 09.04.2019
 *      Author: Max F
 */

#include "calculation.h"
#include "globvars.h"
#include "main.h"



void umrechnung_position (void)
{
	motor_position[0] = adc[0] * 100 / 3724;// 4096*3/3,3 = 3724 --> 10 V am Ventilmotor entsprechen 3V --> 3V --> 3724
	motor_position[1] = adc[1] * 100 / 3724;// 4096*3/3,3 = 3724 --> 10 V am Ventilmotor entsprechen 3V --> 3V --> 3724
	motor_position[2] = adc[2] * 100 / 3724;// 4096*3/3,3 = 3724 --> 10 V am Ventilmotor entsprechen 3V --> 3V --> 3724

}

void position_init (void)
{	umrechnung_position();
	motor_stellung[0] = motor_position[0];
	motor_stellung[1] = motor_position[1];
	motor_stellung[2] = motor_position[2];

	motor_control = 1;
}

void motor_stellen (GPIO_PinState DK)
{	uint32_t PWM_Motor[3];

	//�bergabe der Werte die eingestellt sind zu den zu einstellenden machen
	if (DK == 1)
	switch (motor_control) {
		case (1):
				motor_stellung[0] = motor_stellung_neu[0];
			break;
		case (2):
				motor_stellung[1] = motor_stellung_neu[1];
			break;
		case (3):
				motor_stellung[2] = motor_stellung_neu[2];
			break;
	}




	if (motor_stellung[0] == 0){
		PWM_Motor[0] = 1;
	}else {
		PWM_Motor[0] = 9286 * motor_stellung[0] / 100;
	}

	if (motor_stellung[0] == 100){
		PWM_Motor[0] = 9500;
	}
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, PWM_Motor[0]); //Set PWM dutycycle


	if (motor_stellung[1] == 0){
		PWM_Motor[1] = 1;
	}else {
		PWM_Motor[1] = 9286 * motor_stellung[1] / 100;
	}

	if (motor_stellung[1] == 100){
		PWM_Motor[1] = 9500;
	}
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, PWM_Motor[1]); //Set PWM dutycycle


	if (motor_stellung[2] == 0){
		PWM_Motor[2] = 1;
	}else {
		PWM_Motor[2] = 9286 * motor_stellung[2] / 100;
	}

	if (motor_stellung[2] == 100){
		PWM_Motor[2] = 9500;
	}
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, PWM_Motor[2]); //Set PWM dutycycle
}
