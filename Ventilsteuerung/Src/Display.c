/*
 * Display.c
 *
 *  Created on: 09.04.2019
 *      Author: Max F
 */

#include "main.h"
#include "stm32f4xx_hal.h"
#include "Display.h"
#include "globvars.h"


void wait (unsigned char waitstates)
{
	unsigned char i=0;
	for (i=0; i<waitstates; i++)
	{
		asm("nop");				// nop: 'no operation'
	}
}

void DisplayInit (void)
{

	writeInsDisplay(0x39); 			//function set european chararacter set
	HAL_Delay(10);
	writeInsDisplay(0x39); 			//function set european chararacter set
	HAL_Delay(10);
	writeInsDisplay(0x39); 			//function set european chararacter set
	HAL_Delay(10);
	writeInsDisplay(0x39); 			//function set european chararacter set
	HAL_Delay(10);
	writeInsDisplay(0x08); 			//display off
	HAL_Delay(10);
	writeInsDisplay(0x01); 			//clear display
	HAL_Delay(10);
	writeInsDisplay(0x06); 			//entry mode set increment cursor by 1 not shifting display
	HAL_Delay(10);
	writeInsDisplay(0x17); 			//Character mode and internal power on
	HAL_Delay(10);
	writeInsDisplay(0x10);			//Cursor Move and direction
	HAL_Delay(10);
	writeInsDisplay(0x01); 			//clear display
	HAL_Delay(10);
	writeInsDisplay(0x02); 			//return home
	HAL_Delay(10);
	writeInsDisplay(0x0F); 			//display on


	unsigned char Strich_rechts[8] 			= {0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen
	unsigned char Strich_oben[8]			= {0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen
	unsigned char Grad[8] 					= {0x1C, 0x14, 0x1C, 0x00, 0x00, 0x00, 0x00, 0x00};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen
	unsigned char Doppelstrich_rechts[8]	= {0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen
	unsigned char Dreifachstrich_rechts[8]	= {0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen
	unsigned char Vierfachstrich_rechts[8]	= {0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen
	unsigned char Strich_unten[8]			= {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen
	unsigned char Strich_links[8]			= {0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10};

	defineCharacter(Nr_Strich_rechts, Strich_rechts);													// Definieren des entsprechenden Zeichens und definieren des Speicherortes ('0')
	defineCharacter(Nr_Doppelstrich_rechts, Doppelstrich_rechts);										// Definieren des entsprechenden Zeichens und definieren des Speicherortes ('3')
	defineCharacter(Nr_Dreifachstrich_rechts, Dreifachstrich_rechts);									// Definieren des entsprechenden Zeichens und definieren des Speicherortes ('4')
	defineCharacter(Nr_Vierfachstrich_rechts, Vierfachstrich_rechts);									// Definieren des entsprechenden Zeichens und definieren des Speicherortes ('5')
	defineCharacter(Nr_Strich_oben, Strich_oben);														// Definieren des entsprechenden Zeichens und definieren des Speicherortes ('1')
	defineCharacter(Nr_Grad, Grad);																		// Definieren des entsprechenden Zeichens und definieren des Speicherortes ('2')
	defineCharacter(Nr_Strich_unten, Strich_unten);
	defineCharacter(Nr_Strich_links, Strich_links);
}

void writeInsDisplay(char DisplayData)
{
//	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);

	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_RESET);
	GPIOD->ODR = (DisplayData<<8);

	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
	wait(16);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);

//	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);
//	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_RESET);
//	char RotData = drehen(DisplayData);
//	uint32_t DD = DisplayData;
//	GPIOD->ODR = (DD<<8);
//	HAL_Delay(10);
//	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
//	HAL_Delay(400);
//	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
}

void  writeDataDisplay (char DisplayData)
{

	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);

//	char RotData = drehen(DisplayData);
//	uint32_t DD = DisplayData;
	GPIOD->ODR = (DisplayData<<8);
//	HAL_Delay(10);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
//	HAL_Delay(30);
	wait(160);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);

}

void writeCharDisplay (char character)
{
	writeDataDisplay(character);
}

void writeStringDisplay (const char * string)
{
	do
	{
		writeDataDisplay(*string++);
	}while(*string);
}

void setPositionDisplay (char pos)
{
	writeInsDisplay(LCD_HOME_L1+pos);
}

void DisplayOnOff (char DisplayData)
{
	writeInsDisplay(0x08 + DisplayData);
}

void clearDisplay (void)
{
	writeInsDisplay(0x01);
	setPositionDisplay(LINE1);
}

void defineCharacter (unsigned char position, unsigned char *data)
{
	unsigned char i=0;
	writeInsDisplay(0x40+8*position);

	for (i=0; i<8; i++)
	{
		writeDataDisplay(data[i]);
	}
//	setPositionDisplay(LINE1);
}

void sendDisplayDigit(uint32_t value)
{
/*
 * Runterbrechen eines vorzeichenunbehafteten Integer-Wertes einer beliebigen Variable mit maximal 32bit auf die einzelnen Ziffern, da diese
 * separat an das Display �bertragen werden m�ssen.
 * Anschlie�endes �bertragen der Ziffern an das Display.
*/
	uint8_t value_unit 				= 0;
	uint8_t value_ten 				= 0;
	uint8_t value_hundred 			= 0;
	uint8_t value_thousand 			= 0;
	uint8_t value_tenthousand 		= 0;
	uint8_t value_hundredthousand 	= 0;
	uint8_t value_million 			= 0;
	uint8_t value_tenmillion		= 0;

	if(value >= 10000000)
	{
		value_tenmillion = value / 10000000;
		value -= value_tenmillion * 10000000;
		writeCharDisplay(48+value_tenmillion);

		if(value < 1000000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 1000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 1000000)
	{
		value_million = value / 1000000;
		value -= value_million * 1000000;
		writeCharDisplay(48+value_million);

		if(value < 100000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 1000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 100000)
	{
		value_hundredthousand = value / 100000;
		value -= value_hundredthousand * 100000;
		writeCharDisplay(48+value_hundredthousand);

		if(value < 10000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 1000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 10000)
	{
		value_tenthousand = value / 10000;
		value -= value_tenthousand * 10000;
		writeCharDisplay(48+value_tenthousand);

		if(value < 1000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 1000)
	{
		value_thousand = value / 1000;
		value -= value_thousand * 1000;
		writeCharDisplay(48+value_thousand);

		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 100)
	{
		value_hundred = value / 100;
		value -= value_hundred * 100;
		writeCharDisplay(48+value_hundred);

		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 10)
		value_ten = value / 10,
		value -= value_ten * 10,
		writeCharDisplay(48+value_ten);

	if(value >= 0)
		value_unit = value,
		writeCharDisplay(48+value_unit);
}

void sendDisplayDecimal(uint8_t parameter, uint16_t value)
{
	float parameter_value_real	 		= value * status.Faktor[parameter];
	uint16_t parameter_value_unit		= parameter_value_real;
	uint16_t parameter_value_decimal 	= value - (parameter_value_unit / status.Faktor[parameter]);
	uint16_t faktor_inverse				= (1 / status.Faktor[parameter]);

	sendDisplayDigit(parameter_value_unit);
	writeStringDisplay(",");
	switch(faktor_inverse)
	{
		case (1000):
			if(parameter_value_decimal < 1000)
			{
				writeCharDisplay(48+0);
				if(parameter_value_decimal < 100)
				{
					writeCharDisplay(48+0);
					if(parameter_value_decimal < 10)
						writeCharDisplay(48+0);
				}
			}
		sendDisplayDigit(parameter_value_decimal);
		break;

		case(100):
			if(parameter_value_decimal < 100)
			{
				writeCharDisplay(48+0);
				if(parameter_value_decimal < 10)
					writeCharDisplay(48+0);
			}
			sendDisplayDigit(parameter_value_decimal);
		break;

		case(10):
			if(parameter_value_decimal < 10)
					writeCharDisplay(48+0);
			sendDisplayDigit(parameter_value_decimal);
		break;

		case(1):
				sendDisplayDigit(parameter_value_decimal);
		break;

		default: sendDisplayDigit(parameter_value_decimal);
		break;
	}

}

void show_Menue()
{
	//L�schen des aktuellen Displays
	//clearDisplay();

	//HAL_GPIO_TogglePin(GPIOC, LED2_Pin);

	setPositionDisplay(LINE1);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE2);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE3);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE4);
	writeStringDisplay("                    ");
	wait(16);

	//je nach Position der Drehregler wird Men� ausgew�hlt
	switch(IG_Pos)
	{
	case(Menue_1): //Men� manuelle Steuerung
		Ventil_Menue();

		motor_stellen(DK_1); //Stellen der Motoren nach aktueller Einstellung
	break;

	case(Menue_2):

	break;

	case(Menue_3):

	break;

	case(Menue_4):

	break;

	case(Menue_5):

	break;

	case(Menue_6):

	break;

	case(Menue_7):

	break;

	case(Menue_8):

	break;

	case(Menue_9):

	break;

	case(Menue_10):

	break;

	case(Menue_11):

	break;

	case(Menue_12):

	break;
	}

}

void Ventil_Menue(void) //Men� manuelle Steuerung
{
	uint8_t valuebuffer = Rotary % 101;

	switch (motor_control) {
			case (1):
				motor_stellung_neu[0] = valuebuffer;
				break;
			case (2):
				motor_stellung_neu[1] = valuebuffer;
				break;
			case (3):
				motor_stellung_neu[2] = valuebuffer;
				break;
	}


	setPositionDisplay(LINE1);
	writeStringDisplay("     Akt. Stellt Neu ");
	setPositionDisplay(LINE2);
	writeStringDisplay("Mot1");
	setPositionDisplay(LINE2 + 0x06);
	sendDisplayDigit(motor_position[0]);

	setPositionDisplay(LINE2 + 0x0C);
	sendDisplayDigit(motor_stellung[0]);

	setPositionDisplay(LINE2 + 0x11);
	sendDisplayDigit(motor_stellung_neu[0]);


	setPositionDisplay(LINE3);
	writeStringDisplay("Mot2");
	setPositionDisplay(LINE3 + 0x06);
	sendDisplayDigit(motor_position[1]);
	setPositionDisplay(LINE3 + 0x0C);
	sendDisplayDigit(motor_stellung[1]);
	setPositionDisplay(LINE3 + 0x11);
	sendDisplayDigit(motor_stellung_neu[1]);

	setPositionDisplay(LINE4);
	writeStringDisplay("Mot3");
	setPositionDisplay(LINE4 + 0x06);
	sendDisplayDigit(motor_position[2]);
	setPositionDisplay(LINE4 + 0x0C);
	sendDisplayDigit(motor_stellung[2]);
	setPositionDisplay(LINE4 + 0x11);
	sendDisplayDigit(motor_stellung_neu[2]);


	switch (motor_control) {
			case (1):
				setPositionDisplay(LINE2 + 0x10);
				break;
			case (2):
				setPositionDisplay(LINE3 + 0x10);
				break;
			case (3):
				setPositionDisplay(LINE4 + 0x10);
				break;
	}
}

//weitere Men�s k�nnen definiert werden
