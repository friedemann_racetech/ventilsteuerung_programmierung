/*
 * calculation.h
 *
 *  Created on: 16.05.2019
 *      Author: Max F
 */

#ifndef CALCULATION_H_
#define CALCULATION_H_

#include "main.h"

void umrechnung_position					(void);
void position_init							(void);
void motor_stellen							(GPIO_PinState DK);

#endif /* CALCULATION_H_ */
