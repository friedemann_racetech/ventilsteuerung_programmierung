/*
 * globvars.h
 *
 *  Created on: 09.04.2019
 *      Author: Max F
 */

#ifndef GLOBVARS_H_
#define GLOBVARS_H_

#include <stdint.h>
#include "main.h"

#define Menue_1 1
#define Menue_2 2
#define Menue_3 4
#define Menue_4 8
#define Menue_5 16
#define Menue_6 32
#define Menue_7 64
#define Menue_8 128
#define Menue_9 256
#define Menue_10 512
#define Menue_11 1024
#define Menue_12 2048

#define ADC_Max 4096

#define Offset 3 		//Abweichung zwischen zu stellender Position und gestellter Position


uint16_t IG_Pos;	//Position des Inkrementalgebers
uint16_t motor_position[3]; //aktuelle Position der Motoren in Prozent 0-100
uint16_t motor_stellung[3]; //aktuell gew�nschte Position des Motors in Prozent
uint16_t motor_stellung_neu[3]; //neue gew�nschte Position des Motors, wo der Motor nach best�tigen hinf�hrt in Prozent (dieser muss f�r �nderung ge�ndert werden)
uint8_t motor_control;	//an welchem Motor werden Positions�nderungen durchgef�hrt
uint8_t motor_control_changes; //Motor wurde ge�ndert (wird erst wieder zur�ckgesetzt, wenn Knopf losgelassen

uint32_t adc[5], buffer[5];	//Variablen Auslesen ADC

ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim3;

//Druckkn�pfe Variablen
GPIO_PinState DG_Best;
GPIO_PinState DK_1;
GPIO_PinState DK_2;

int Rotary;


struct	Parameter	status;

struct Parameter
{
	uint8_t 		valChanged;
	uint16_t	 	MinMax[4][22];
	double			Faktor[22];
};

enum DisplayCharacters
{
	Nr_Strich_rechts			= 0,
	Nr_Strich_oben				= 1,
	Nr_Grad						= 2,
	Nr_Doppelstrich_rechts		= 3,
	Nr_Dreifachstrich_rechts 	= 4,
	Nr_Vierfachstrich_rechts 	= 5,
	Nr_Strich_unten				= 6,
	Nr_Strich_links				= 7
};

#endif /* GLOBVARS_H_ */
