/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define Ersatz6_Pin GPIO_PIN_15
#define Ersatz6_GPIO_Port GPIOC
#define LED2_Pin GPIO_PIN_0
#define LED2_GPIO_Port GPIOC
#define LED1_Pin GPIO_PIN_1
#define LED1_GPIO_Port GPIOC
#define Mot1_Position_Pin GPIO_PIN_0
#define Mot1_Position_GPIO_Port GPIOA
#define Mot2_Position_Pin GPIO_PIN_1
#define Mot2_Position_GPIO_Port GPIOA
#define Mot3_Position_Pin GPIO_PIN_2
#define Mot3_Position_GPIO_Port GPIOA
#define Volumenstrom_in_Pin GPIO_PIN_3
#define Volumenstrom_in_GPIO_Port GPIOA
#define Ersatz_in_Pin GPIO_PIN_4
#define Ersatz_in_GPIO_Port GPIOA
#define Pumpen_Relais_Pin GPIO_PIN_5
#define Pumpen_Relais_GPIO_Port GPIOA
#define Drehgeber1_Pin GPIO_PIN_6
#define Drehgeber1_GPIO_Port GPIOA
#define Drehgeber2_Pin GPIO_PIN_7
#define Drehgeber2_GPIO_Port GPIOA
#define IG0_Pin GPIO_PIN_0
#define IG0_GPIO_Port GPIOB
#define Ersatz4_Pin GPIO_PIN_1
#define Ersatz4_GPIO_Port GPIOB
#define Ersatz5_Pin GPIO_PIN_2
#define Ersatz5_GPIO_Port GPIOB
#define Motor1_Steuerung_Pin GPIO_PIN_9
#define Motor1_Steuerung_GPIO_Port GPIOE
#define Motor2_Steuerung_Pin GPIO_PIN_11
#define Motor2_Steuerung_GPIO_Port GPIOE
#define Motor3_Steuerung_Pin GPIO_PIN_13
#define Motor3_Steuerung_GPIO_Port GPIOE
#define PWM_Eratz_Pin GPIO_PIN_14
#define PWM_Eratz_GPIO_Port GPIOE
#define IG6_Pin GPIO_PIN_10
#define IG6_GPIO_Port GPIOB
#define IG7_Pin GPIO_PIN_11
#define IG7_GPIO_Port GPIOB
#define IG8_Pin GPIO_PIN_12
#define IG8_GPIO_Port GPIOB
#define IG9_Pin GPIO_PIN_13
#define IG9_GPIO_Port GPIOB
#define IG10_Pin GPIO_PIN_14
#define IG10_GPIO_Port GPIOB
#define IG11_Pin GPIO_PIN_15
#define IG11_GPIO_Port GPIOB
#define LCD0_Pin GPIO_PIN_8
#define LCD0_GPIO_Port GPIOD
#define LCD1_Pin GPIO_PIN_9
#define LCD1_GPIO_Port GPIOD
#define LCD2_Pin GPIO_PIN_10
#define LCD2_GPIO_Port GPIOD
#define LCD3_Pin GPIO_PIN_11
#define LCD3_GPIO_Port GPIOD
#define LCD4_Pin GPIO_PIN_12
#define LCD4_GPIO_Port GPIOD
#define LCD5_Pin GPIO_PIN_13
#define LCD5_GPIO_Port GPIOD
#define LCD6_Pin GPIO_PIN_14
#define LCD6_GPIO_Port GPIOD
#define LCD7_Pin GPIO_PIN_15
#define LCD7_GPIO_Port GPIOD
#define LCD_R_W_Pin GPIO_PIN_7
#define LCD_R_W_GPIO_Port GPIOC
#define LCD_EN_Pin GPIO_PIN_8
#define LCD_EN_GPIO_Port GPIOC
#define Ersatz1_Pin GPIO_PIN_8
#define Ersatz1_GPIO_Port GPIOA
#define Ersatz2_Pin GPIO_PIN_9
#define Ersatz2_GPIO_Port GPIOA
#define Ersatz3_Pin GPIO_PIN_10
#define Ersatz3_GPIO_Port GPIOA
#define LCD_RS_Pin GPIO_PIN_12
#define LCD_RS_GPIO_Port GPIOC
#define DG_Best_Pin GPIO_PIN_2
#define DG_Best_GPIO_Port GPIOD
#define DK1_Pin GPIO_PIN_3
#define DK1_GPIO_Port GPIOD
#define DK2_Pin GPIO_PIN_4
#define DK2_GPIO_Port GPIOD
#define Debounce_DK_En_Pin GPIO_PIN_5
#define Debounce_DK_En_GPIO_Port GPIOD
#define Debounce_DK_In_Pin GPIO_PIN_6
#define Debounce_DK_In_GPIO_Port GPIOD
#define IG1_Pin GPIO_PIN_5
#define IG1_GPIO_Port GPIOB
#define IG2_Pin GPIO_PIN_6
#define IG2_GPIO_Port GPIOB
#define IG3_Pin GPIO_PIN_7
#define IG3_GPIO_Port GPIOB
#define IG4_Pin GPIO_PIN_8
#define IG4_GPIO_Port GPIOB
#define IG5_Pin GPIO_PIN_9
#define IG5_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
