/*
 * Display.h
 *
 *  Created on: 09.04.2019
 *      Author: Max F
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

//#include "Dashboard.h"

#define MaxLines				4			//Anzahl der Linien im Display
#define LCD_Characters			20			//Anzahl der Zeichen pro Linie
#define LCD_HOME_L1	0x80
#define LINE1	0
#define LINE2	LINE1+0x40
#define LINE3	LINE1+0x14
#define	LINE4 	LINE2+0x14

// Clear Display -------------- 0b00000001
#define LCD_CLEAR_DISPLAY       0x01

// Cursor Home ---------------- 0b0000001x
#define LCD_CURSOR_HOME         0x02

// Set Entry Mode ------------- 0b000001xx
#define LCD_SET_ENTRY           0x04

#define LCD_ENTRY_DECREASE      0x00
#define LCD_ENTRY_INCREASE      0x02
#define LCD_ENTRY_NOSHIFT       0x00
#define LCD_ENTRY_SHIFT         0x01

// Set Display ---------------- 0b00001xxx
#define LCD_SET_DISPLAY         0x08

#define LCD_DISPLAY_OFF         0x00
#define LCD_DISPLAY_ON          0x04
#define LCD_CURSOR_OFF          0x00
#define LCD_CURSOR_ON           0x02
#define LCD_BLINKING_OFF        0x00
#define LCD_BLINKING_ON         0x01

// Set Shift ------------------ 0b0001xxxx
#define LCD_SET_SHIFT           0x10

#define LCD_CURSOR_MOVE         0x00
#define LCD_DISPLAY_SHIFT       0x08
#define LCD_SHIFT_LEFT          0x00
#define LCD_SHIFT_RIGHT         0x04

// Set Function --------------- 0b001xxxxx
#define LCD_SET_FUNCTION        0x20

#define LCD_FUNCTION_4BIT       0x00
#define LCD_FUNCTION_8BIT       0x10
#define LCD_FUNCTION_1LINE      0x00
#define LCD_FUNCTION_2LINE      0x08
#define LCD_FUNCTION_5X7        0x00
#define LCD_FUNCTION_5X10       0x04

#define LCD_SOFT_RESET          0x30

#define GETCURSORADDR()	CheckBusy();


//Funktionsprototypen

void initDisplay					(void);
void wait 							(unsigned char waitstates);
void writeCharDisplay				(char character);
void writeStringDisplay				(const char * string);
void setSymbolDisplay				(char add, char data);
void setPositionDisplay				(char pos);
void DisplayOnOff					(char data);
void defineCharacter				(unsigned char position, unsigned char *data);
void clearDisplay					(void);
void initDisplay_2					(void);

void writeDataDisplay				(char data);
void writeInsDisplay				(char ins);

unsigned char CheckBusy				(void);

void sendDisplayDigit				(uint32_t value);
void sendDisplayDecimal				(uint8_t parameter, uint16_t value);

void show_Menue						(void);
void Ventil_Menue					(void);



#endif /* DISPLAY_H_ */
